const NO_OF_IMAGES = 6;
let SLIDER = 0;

/*Slider*/
function showSlide(index) {
  SLIDER = index;
  let width = document.getElementsByClassName("slider-img")[0].offsetWidth;
  let container = document.getElementsByClassName("slide-img-container")[0];
  let buttons = document.getElementsByClassName("slider-button");
  container.style.transition = "transform 0.8s ease-in-out";
  container.style.transform = "translateX(" + -width * index + "px)";

  for (let i = 0; i < NO_OF_IMAGES; i++) {
    buttons[i].style.backgroundColor = "rgb(44, 131, 176)";
  }
  buttons[index].style.backgroundColor = "rgb(163, 163, 163)";
}

function showRandomSlide() {
  showSlide(Math.floor(Math.random() * NO_OF_IMAGES));
}
showRandomSlide();

window.setInterval(function () {
  showSlide(SLIDER);
  SLIDER += 1;
  if (SLIDER >= NO_OF_IMAGES) {
    SLIDER = 0;
  }
}, 2000);

/* Article page functions*/
let PAGE_INDEX = 0;
let N_PAGES = 3;
let ARTICLES_PR_PAGE = 3;
function readMore(artIndex) {
  let articles = document.getElementsByClassName("article");
  let hiddenText = articles[artIndex].getElementsByClassName("hidden-text")[0];
  let button = articles[artIndex].getElementsByTagName("button")[0];
  let dots = articles[artIndex].getElementsByClassName("three-dots")[0];
  if (!hiddenText.style.display) {
    hiddenText.style.display = "inline";
    dots.style.display = "none";
    button.style.display = "none";
  }
}

function hideAllText(pageIndex) {
  let statIndex = pageIndex * ARTICLES_PR_PAGE;
  let maxIndex = pageIndex * ARTICLES_PR_PAGE + ARTICLES_PR_PAGE;
  let articles = document.getElementsByClassName("article");
  for (let i = pageIndex * ARTICLES_PR_PAGE; i < maxIndex; i++) {
    let hiddenText = articles[i].getElementsByClassName("hidden-text")[0];
    let button = articles[i].getElementsByTagName("button")[0];
    let dots = articles[i].getElementsByClassName("three-dots")[0];
    button.style.display = "inline";
    dots.style.display = "inline";
    hiddenText.style.display = null;
  }
}

function changePage(pageIndex) {
  let pages = document.getElementsByClassName("article-container");
  let buttons = document.getElementsByClassName("page-button");
  let span = document.getElementsByClassName("page-of")[0];
  for (let i = 0; i < pages.length; i++) {
    pages[i].style.display = "none";
    buttons[i].classList.remove("page-button-active");
  }
  pages[pageIndex].style.display = "block";
  buttons[pageIndex].classList.add("page-button-active");
  span.innerHTML = "Page " + (pageIndex + 1) + " of " + N_PAGES;
  hideAllText(pageIndex);
  PAGE_INDEX = pageIndex;
}

function nextPage() {
  if (PAGE_INDEX < N_PAGES - 1) {
    PAGE_INDEX += 1;
    changePage(PAGE_INDEX);
  }
}

//Search functionality

$(function () {
  $("input")
    .on("input.highlight", function () {
      // Determine specified search term
      var searchTerm = $(this).val();
      // Highlight search term inside a specific context
      $("#context").unmark().mark(searchTerm);
    })
    .trigger("input.highlight")
    .focus();
});
